<?php

namespace App\Repository;

use App\Entity\DespreNoi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DespreNoi|null find($id, $lockMode = null, $lockVersion = null)
 * @method DespreNoi|null findOneBy(array $criteria, array $orderBy = null)
 * @method DespreNoi[]    findAll()
 * @method DespreNoi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DespreNoiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DespreNoi::class);
    }

    // /**
    //  * @return DespreNoi[] Returns an array of DespreNoi objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DespreNoi
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
