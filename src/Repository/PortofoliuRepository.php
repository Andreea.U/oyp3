<?php

namespace App\Repository;

use App\Entity\Portofoliu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Portofoliu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Portofoliu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Portofoliu[]    findAll()
 * @method Portofoliu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PortofoliuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Portofoliu::class);
    }

    // /**
    //  * @return Portofoliu[] Returns an array of Portofoliu objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Portofoliu
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
