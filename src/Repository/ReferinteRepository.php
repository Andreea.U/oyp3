<?php

namespace App\Repository;

use App\Entity\Referinte;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Referinte|null find($id, $lockMode = null, $lockVersion = null)
 * @method Referinte|null findOneBy(array $criteria, array $orderBy = null)
 * @method Referinte[]    findAll()
 * @method Referinte[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferinteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Referinte::class);
    }

    // /**
    //  * @return Referinte[] Returns an array of Referinte objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Referinte
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
