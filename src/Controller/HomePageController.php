<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Servicii;
use App\Entity\DespreNoi;
use App\Entity\Referinte;
use App\Entity\Contact;
use App\Entity\Portofoliu;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpClient\HttpClient;

class HomePageController extends AbstractController
{
    /**
     * @Route("/home", name="home_page")
     */
    public function new(Request $request)
    {
        $client = HttpClient::create();
        $response = $client->request('GET','http://127.0.0.1:8001/api/referintes.json');

        $servicii = $this->getDoctrine()->getRepository(Servicii::class)->findAll();
        $desprenoi = $this->getDoctrine()->getRepository(DespreNoi::class)->findAll();
        $portofoliu = $this->getDoctrine()->getRepository(Portofoliu::class)->findAll();

        $contact=new Contact();
        $referinte = $response->toArray();


        $form = $this->createFormBuilder($contact)
        ->add('nume', TextType::class, [
            'attr' => [ 
                'class' => 'mt-5',
                'value' => ' '
                ]
        ])
        ->add('prenume', TextType::class, [
            'attr' => [ 
                'class' => 'mt-5',
                'value' => ' '
            ]
        ])
        ->add('email', TextType::class, [
            'attr' => [ 
                'class' => 'mt-5',
                'value' => ' '
                ]
        ])
        ->add('save', SubmitType::class, [
            'label' => 'Contacteaza-ne',
            'attr' => [ 'class' => 'btn nav-background text-white font-weight-bold mt-5']
            ])
        ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $contact = $form->getData();
            $enitityManager = $this->getDoctrine()->getManager();
            $enitityManager->persist($contact);
            $enitityManager->flush();
        } 

        return $this->render('home_page/index.html.twig', [
            'servicii' => $servicii,
            'desprenoi' => $desprenoi,
            'referinte' => $referinte,
            'form' => $form->createView(),
            'poze' => $portofoliu
        ]);
    }
}
